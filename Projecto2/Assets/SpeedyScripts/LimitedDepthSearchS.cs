﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LimitedDepthSearchS : SearchAlgorithm {

	private Stack<SearchNode> openStack = new Stack<SearchNode> ();
	private Dictionary<object,int> closedSet = new Dictionary<object, int> ();

	public int depthLimit = 8;

	void Start () 
	{
		Debug.Log("Limited Depth Search");

		problem = GameObject.Find ("Map").GetComponent<Map> ().GetProblem();
		SearchNode start = new SearchNode (problem.GetStartState (), 0);
		openStack.Push (start);
		closedSet.Add (start.state, 0);
	}
	
	protected override void Step()
	{
		if (openStack.Count > 0)
		{
			if(maxDatasetSize < openStack.Count + closedSet.Count)
				maxDatasetSize = openStack.Count + closedSet.Count;

			SearchNode cur_node = openStack.Pop();

			if (problem.IsGoal (cur_node.state)) {
				solution = cur_node;
				finished = true;
				running = false;

			} else 
			{
				Successor[] sucessors = problem.GetSuccessors (cur_node.state);
				if (cur_node.depth < depthLimit) {
					for (int i = sucessors.Length - 1; i >= 0; i--) {
						if(closedSet.ContainsKey (sucessors[i].state)){
							if(closedSet [sucessors[i].state] > cur_node.depth + 1){
								SearchNode new_node = new SearchNode (sucessors[i].state, sucessors[i].cost + cur_node.g, sucessors[i].action, cur_node);
								openStack.Push (new_node);
								closedSet [sucessors[i].state] = cur_node.depth + 1;
							}
						}
						else {
							SearchNode new_node = new SearchNode (sucessors[i].state, sucessors[i].cost + cur_node.g, sucessors[i].action, cur_node);
							openStack.Push (new_node);
							closedSet.Add (new_node.state, new_node.depth);
						}
					}
				}
			}
		
		}
		else
		{
			finished = true;
			running = false;
		}
	}
}
