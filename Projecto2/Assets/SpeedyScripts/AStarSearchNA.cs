﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AStarSearchNA : SearchAlgorithm {

	private priorityQueue<SearchNode> openQueue = new priorityQueue<SearchNode> ();
	private Dictionary<object,int> closedSet = new Dictionary<object, int> ();

	void Start () 
	{
		Debug.Log("A* Search with Non-admissible Heuristic");

		problem = GameObject.Find ("Map").GetComponent<Map> ().GetProblem ();
		object state = (object)problem.GetStartState ();
		SearchNode start = new SearchNode (state, 0, problem.getNonHeuristic (state));
		openQueue.Enqueue(start,(int)start.f);
		closedSet.Add (state, 0);
	}
	
	protected override void Step()
	{
		if (openQueue.Count > 0)
		{
			if(maxDatasetSize < openQueue.Count + closedSet.Count)
				maxDatasetSize = openQueue.Count + closedSet.Count;
			
			SearchNode cur_node = openQueue.Dequeue ();

			if (problem.IsGoal (cur_node.state)) {
				solution = cur_node;
				finished = true;
				running = false;
			} else {
				Successor[] sucessors = problem.GetSuccessors (cur_node.state);
				foreach (Successor suc in sucessors) {
					if(closedSet.ContainsKey (suc.state))
					{
						if(closedSet [suc.state] > cur_node.depth + 1)
						{
							SearchNode new_node = new SearchNode (suc.state, suc.cost + cur_node.g, problem.getNonHeuristic(suc.state), suc.action, cur_node);
							openQueue.Enqueue (new_node,  (int)new_node.f);
							closedSet [suc.state] = cur_node.depth + 1;

						}
					}
					else  {
						SearchNode new_node = new SearchNode (suc.state, suc.cost + cur_node.g, problem.getNonHeuristic(suc.state), suc.action, cur_node);

						openQueue.Enqueue (new_node, (int)new_node.f);
						closedSet.Add (suc.state, new_node.depth);
					}
				}
			}
		}
		else
		{
			finished = true;
			running = false;
		}
	}
}
