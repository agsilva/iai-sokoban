﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*public class myComparerf:IComparer<SearchNode>
{
	public int Compare( SearchNode a, SearchNode b)
	{
		return (int) (a.f - b.f);
	}
}*/

public class AStarSearch : SearchAlgorithm {

	private priorityQueue<SearchNode> openQueue = new priorityQueue<SearchNode> ();
	private Dictionary<object,int> closedSet = new Dictionary<object, int> ();
	public List heuristic; // First Heuristic, Admissible, Non-admissible
	private int hv; // heuristic value = 0 -> FirstHeuristic, hv = 1 -> AdmissibleHeuristic, hv = 2 -> NA Heuristic 
	/*private myComparerf c;*/

	void Start () 
	{
		if (heuristic.ToString ().Equals ("First")) {
			hv = 0;
			Debug.Log("A* Search with First Heuristic");
		}
		else {
			if (heuristic.ToString ().Equals ("Admissible")) {
				hv = 1;
				Debug.Log ("A* Search with Admissible Heuristic");
			} else {
				hv = 2;
				Debug.Log("A* Search with Non-admissible Heuristic");
			}
		}

		problem = GameObject.Find ("Map").GetComponent<Map> ().GetProblem ();
		object state = (object)problem.GetStartState ();
		SearchNode start = new SearchNode (state, 0, problem.getHeuristic (state,hv));
		/*c = new myComparerf ();*/
		openQueue.Enqueue(start,(int)start.f);
		closedSet.Add (state, 0);
	}
	
	protected override void Step()
	{
		if (openQueue.Count > 0)
		{
			if(maxDatasetSize < openQueue.Count + closedSet.Count)
				maxDatasetSize = openQueue.Count + closedSet.Count;
			
			SearchNode cur_node = openQueue.Dequeue ();

			if (debug) {
				SokobanState state = (SokobanState)cur_node.state;

				//Debug.Log (state.player + " -> >" + cur_node.f + "<  move = " + cur_node.action + "\nminbox -> " + ((SokobanProblem)problem).getMinCrateDistance(state) + "   goals = " +((SokobanProblem)problem).getRemainingGoals(state) 
				//	+ "   blocked -> " + ((SokobanProblem)problem).getBlockedCrates(state) + "   distToGoal = " +((SokobanProblem)problem).getGoalDistance(state));
				Object last = GameObject.FindGameObjectWithTag ("Debug");
				GameObject.Destroy (last);
				Instantiate (myPrefab, state.player * GameObject.Find ("Map").GetComponent<Map> ().cellSize, Quaternion.identity);	
				Object[] crates = GameObject.FindGameObjectsWithTag ("Crate");
				foreach (Object crate in crates)
					GameObject.Destroy (crate);
				foreach (Vector2 crate in state.crates)
				{
					Instantiate (GameObject.Find("Map").GetComponent<Map>().cratePrefab, crate * GameObject.Find ("Map").GetComponent<Map> ().cellSize, Quaternion.identity);	
				}
			}


			if (!optimized && !closedSet.ContainsKey (cur_node.state))
				closedSet.Add (cur_node.state, cur_node.depth);


			if (problem.IsGoal (cur_node.state)) {
				solution = cur_node;
				finished = true;
				running = false;
				closedSet.Clear ();
			} else {
				Successor[] sucessors = problem.GetSuccessors (cur_node.state);
				foreach (Successor suc in sucessors) {
					if(closedSet.ContainsKey (suc.state))
					{
						if(closedSet [suc.state] > cur_node.depth + 1)
						{
							//                                               cost = 1f
							SearchNode new_node = new SearchNode (suc.state, suc.cost + cur_node.g, problem.getHeuristic(suc.state, hv), suc.action, cur_node);
							openQueue.Enqueue (new_node,  (int)new_node.f);
							closedSet [suc.state] = cur_node.depth + 1;

						}
					}
					else  {
						//                                               cost = 1f
						SearchNode new_node = new SearchNode (suc.state, suc.cost + cur_node.g, problem.getHeuristic(suc.state, hv), suc.action, cur_node);

						openQueue.Enqueue (new_node, (int)new_node.f);
						closedSet.Add (suc.state, new_node.depth);
					}
				}
			}
		}
		else
		{
			finished = true;
			running = false;
		}
	}

	public enum List {First, Admissible, NonAdmissible}
}
