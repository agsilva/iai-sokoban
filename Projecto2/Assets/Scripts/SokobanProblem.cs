﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SokobanState {

	public List<Vector2> crates;
	public Vector2 player;


	public SokobanState(List<Vector2> crates, Vector2 player)
	{
		this.crates = crates;
		this.player = player;
	}

	// Copy constructor
	public SokobanState(SokobanState other)
	{
		if (other != null) {
			this.crates = new List<Vector2> (other.crates);
			this.player = other.player;
		}
	}

	// Compare two states. Consider that each crate is in the same index in the array for the two states.
	public override bool Equals(System.Object obj)
	{
		if (obj == null) 
		{
			return false;
		}

		SokobanState s = obj as SokobanState;
		if ((System.Object)s == null)
		{
			return false;
		}

		if (player != s.player) {
			return false;
		}
			
		for (int i = 0; i < crates.Count; i++)
		{
			if (crates[i] != s.crates[i])
			{
				return false;
			}
		}

		return true;
	}

	public bool Equals(SokobanState s)
	{
		if ((System.Object)s == null) 
		{
			return false;
		}

		if (player != s.player) {
			return false;
		}

		for (int i = 0; i < crates.Count; i++)
		{
			if (crates[i] != s.crates[i])
			{
				return false;
			}
		}

		return true;
	}

	public override int GetHashCode()
	{
		int hc = crates.Count;
		for(int i = 0; i < crates.Count; i++)
		{
			hc = unchecked(hc * 17 + crates[i].GetHashCode());
		}

		return hc ^ player.GetHashCode ();
	}

	public static bool operator == (SokobanState s1, SokobanState s2)
	{
		// If both are null, or both are same instance, return true.
		if (System.Object.ReferenceEquals(s1, s2))
		{
			return true;
		}

		// If one is null, but not both, return false.
		if (((object)s1 == null) || ((object)s2 == null))
		{
			return false;
		}

		if (s1.player != s2.player) {
			return false;
		}

		for (int i = 0; i < s1.crates.Count; i++)
		{
			if (s1.crates[i] != s2.crates[i])
			{
				return false;
			}
		}

		return true;
	}

	public static bool operator != (SokobanState s1, SokobanState s2)
	{
		return !(s1 == s2);
	}
}


public class SokobanProblem : ISearchProblem {
	
	private bool[,] walls;
	private List<Vector2> goals;
	private SokobanState start_state;
	private Action[] allActions = Actions.GetAll();
	private List<Vector2> nonStoredCrates;

	protected int visited = 0;
	protected int expanded = 0;

	private Map map;

	public SokobanProblem(Map map)
	{
		walls = map.GetWalls ();
		goals = map.GetGoals ();
	
		List<Vector2> crates_copy = new List<Vector2> (map.GetCrates ());
		start_state = new SokobanState (crates_copy, map.GetPlayerStart ());
	}
		
	public object GetStartState()
	{
		return start_state;
	}
		
	public bool IsGoal (object state)
	{
		nonStoredCrates = getStateCrates (state);

		if(nonStoredCrates.Count == 0)
			return true;

		return false;
	}

	/**************************************************************************************************/
	/***                                    ADDED METHODS                                           ***/
	/**************************************************************************************************/

	// Returns current non-stored crates
	public List<Vector2> getStateCrates (object state)
	{
		SokobanState s = (SokobanState)state;
		List<Vector2> stateCrates = new List<Vector2> ();

		foreach (Vector2 crate in s.crates) {
			if (!goals.Contains (crate))
				stateCrates.Add (crate);
		}

		return stateCrates;
	}

	// Returns the distance between the crate and the nearest goal
	public float getCratetoGoalDistance(Vector2 crate, List<Vector2> crates)
	{
		float minDist = 5000;
		float tempdist = 0;
		float x = crate.x;
		float y = crate.y;

		foreach (Vector2 goal in goals) {
			if (!crates.Contains (goal)) {
				tempdist = Mathf.Abs (goal.x - x) + Mathf.Abs (goal.y - y);
				if (tempdist < minDist)
					minDist = tempdist;
			}
		}
		return minDist;
	}

	// Returns the distance between the crate and its nearest non-stored neighbor
	public float getCratetoCrateDistance(Vector2 crate)
	{
		float minDist = 5000;
		float tempdist = 0;
		float x = crate.x;
		float y = crate.y;

		foreach (Vector2 crate2 in nonStoredCrates) {
				tempdist = Mathf.Abs (crate2.x - x) + Mathf.Abs (crate2.y - y);
				if (tempdist < minDist)
					minDist = tempdist;
		}
		return minDist;
	}

	// Returns the sum of all distances between non-stored crates and their nearest non-stored neighbor
	public float getCrateDistance()
	{
		float dist = 0;
		foreach (Vector2 crate in nonStoredCrates) {
			dist += getCratetoCrateDistance (crate);
		}
		return dist;
	}

	// Returns the sum of all distances between non-stored crates and their nearest goal
	public float getGoalDistance(object state) 
	{
		SokobanState s = (SokobanState)state;
		float dist = 0;

		foreach (Vector2 crate in nonStoredCrates) {
			dist += getCratetoGoalDistance (crate, s.crates);
		}

		return dist;
	}

	// Returns the distance between the state and the nearest non-stored crate
	public float getMinCrateDistance(object state)
	{
		SokobanState s = (SokobanState)state;
		float minDist = 5000;
		float tempdist = 0;
		float x = s.player.x;
		float y = s.player.y;

		foreach (Vector2 crate in nonStoredCrates) {
				tempdist = Mathf.Abs (crate.x - x) + Mathf.Abs (crate.y - y);
				if (tempdist < minDist)
					minDist = tempdist;
		}

		if (minDist == 5000)
			return 0;
		
		return minDist;
	}

	// Checks if a crate is stuck (and not stored)
	public int isBlockedCrate(Vector2 crate, bool[,] obstacles)
	{
		int x = (int)crate.x;
		int y = (int)crate.y;
		
		if (((obstacles [y - 1, x] || obstacles [y + 1, x]) && (obstacles [y, x - 1] || (obstacles [y, x + 1]))))
			return 1;
		if (((walls [y - 1, x] || walls [y + 1, x]) && (walls [y, x - 1] || (walls [y, x + 1]))))
			return 100;
		return 0;
	}

	// Returns the number of crates that are stuck (and not stored)
	public int getBlockedCrates()
	{
		int blockedCrates = 0;

		bool[,] obstacles = (bool[,])walls.Clone();
		foreach (Vector2 crate in nonStoredCrates)
			obstacles [(int)crate.y, (int)crate.x] = true;

		foreach (Vector2 crate in nonStoredCrates) {
			blockedCrates += isBlockedCrate (crate, obstacles);
		}

		return blockedCrates;
	}

	// Returns the number of goals that don't have a crate stored in
	public int getRemainingGoals()
	{
		return nonStoredCrates.Count;
	}

	// Meta 2 - Returns NA Heuristic of the problem's current state
	public int getNonHeuristic(object state)
	{
		nonStoredCrates = getStateCrates (state);

		return (int)((getBlockedCrates() * 30 + getRemainingGoals() * 1.5 + (getMinCrateDistance(state)-1) * 3/5 + getGoalDistance(state)*3));
	}

	// Meta 2 - Returns Admissible Heuristic of the problem's current state
	public int getAdmissibleHeuristic(object state)
	{
		nonStoredCrates = getStateCrates (state);

		return (int)((getMinCrateDistance(state)-1) + getGoalDistance(state)) ;
	}

	// Meta 1 - Returns heuristic of the problem's current state i.e. remaining goals
	public int getFirstHeuristic(object state)
	{
		nonStoredCrates = getStateCrates (state);

		return nonStoredCrates.Count;
	}

	// Gets the heuristic chosen by the player
	public int getHeuristic(object state, int h)
	{
		if (h == 0)
			return getFirstHeuristic (state);
		else {
			if (h == 1)
				return getAdmissibleHeuristic (state);
			else
				return getNonHeuristic (state);
		}

	}

	/**************************************************************************************************/
	/**************************************************************************************************/

	public Successor[] GetSuccessors(object state)
	{
		SokobanState s = (SokobanState)state;

		visited++;

		List<Successor> result = new List<Successor> ();

		foreach (Action a in allActions) {
			Vector2 movement = Actions.GetVector (a);

			if (CheckRules(s, movement))
			{
				expanded++;

				SokobanState new_state = new SokobanState (s);

				new_state.player += movement;

				for (int i = 0; i < new_state.crates.Count; i++) {
					if (new_state.crates[i] == new_state.player) {
						new_state.crates[i] += movement;
						break;
					}
				}
					
				result.Add (new Successor (new_state, 1f, a));
			}
		}

		return result.ToArray ();
	}

	public int GetVisited()
	{
		return visited;
	}

	public int GetExpanded()
	{
		return expanded;
	}

	// Method needed when running more than one algorithm script
	public void Reset()
	{
		visited = 0;
		expanded = 0;
	}


	private bool CheckRules(SokobanState state, Vector2 movement)
	{
		Vector2 new_pos = state.player + movement;

		// Move to wall?
		if (walls [(int)new_pos.y, (int)new_pos.x]) {
			return false;
		}

		// Crate in front and able to move?
		int index = state.crates.IndexOf(new_pos);
		if (index != -1) {
			Vector2 new_crate_pos = state.crates [index] + movement;

			if (walls [(int)new_crate_pos.y, (int)new_crate_pos.x]) {
				return false;
			}

			if (state.crates.Contains(new_crate_pos)) {
				return false;
			}
		}

		return true;
	}
}

