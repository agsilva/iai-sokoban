using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;



public enum Action {North, East, South, West, None=-1};

public class Actions
{
	private static readonly Action[] all_actions = { Action.North, Action.East, Action.South, Action.West };

	public static Action[] GetAll()
	{
		return all_actions;
	}

	public static Vector2 GetVector(Action a)
	{
		if (a == Action.North) {
			return new Vector2(0, 1);
		} else if (a == Action.East) {
			return new Vector2(1, 0);
		} else if (a == Action.South) {
			return new Vector2(0, -1);
		} else if (a == Action.West) {
			return new Vector2(-1, 0);
		}
		return new Vector2(0, 0);
	}

	public static Action Reverse(Action dir)
	{
		if (dir == Action.North) {
			return Action.South;
		} else if (dir == Action.East) {
			return Action.West;
		} else if (dir == Action.South) {
			return Action.North;
		} else if (dir == Action.West) {
			return Action.East;
		} else {
			return Action.None;
		}
	}

	public static string ToString(Action a)
	{
		if (a == Action.North) {
			return "North";
		} else if (a == Action.East) {
			return "East";
		} else if (a == Action.South) {
			return "South";
		} else if (a == Action.West) {
			return "West";
		} else {
			return "None";
		}
	}
}


public class SearchAgent : MonoBehaviour {

	private int cellSize;
	private SearchAlgorithm search;
	private SearchAlgorithm[] algorithms;
	private List<Action> path;
	private GameObject[] crates;
	private List<string> strings = new List<string>();
	private string mapName;
	int ind = 0;

	void Start () {
		path = null;

		// Get the cell size from the map.
		cellSize = GameObject.Find("Map").GetComponent<Map>().cellSize;

		algorithms = GameObject.Find ("Map").GetComponents<SearchAlgorithm> ();

		search = algorithms [ind];
		algorithms [ind].enabled = true;

		if(ind > 0)
			algorithms [ind - 1].enabled = false;

		// Get the search algorithm to use from the map
		//search = GameObject.Find ("Map").GetComponent<SearchAlgorithm> ();
		search.StartRunning ();

		//Get the crate objects
		crates = GameObject.FindGameObjectsWithTag ("Crate");

		strings = new List<string>();
		ind++;
	}
	
	// Update is called once per frame
	void Update () {
		if (path == null && search.Finished ()) {
			strings.Add (search.GetType().FullName);
			strings.Add ("Visited: " + search.problem.GetVisited ().ToString ());
			strings.Add  ("Expanded: " + search.problem.GetExpanded ().ToString ());
			strings.Add  ("Max Dataset Size (Memory): " + (search.maxDatasetSize).ToString ());

			Debug.Log ("Visited: " + search.problem.GetVisited ().ToString ());
			Debug.Log ("Expanded: " + search.problem.GetExpanded ().ToString ());
			Debug.Log ("Max Dataset Size (Memory): " + (search.maxDatasetSize).ToString ());
			path = search.GetActionPath ();
			if (path != null) {
				Debug.Log ("Path Length: " + path.Count.ToString ());
				strings.Add ("Path Length: " + path.Count.ToString ());
				Debug.Log ("[" + string.Join (",", path.ConvertAll<string> (Actions.ToString).ToArray()) + "]");
			}
			strings.Add ("");	
			strings.Add ("");

			mapName = GameObject.Find ("Map").GetComponent<Map>().map.name.ToString();
			mapName +=" - results.txt";
			using (StreamWriter writetxt = new StreamWriter (mapName,true))
			{
				foreach (string str in strings)
					writetxt.WriteLine (str); 
			}
			if (ind < algorithms.Length) {
				search.problem.Reset ();
				Start ();
			}
		}
	}

	void FixedUpdate() {
		if (path != null && path.Count > 0) {
			Time.timeScale = 0.05f;
			Action action = path [0];
			path.RemoveAt (0);

			Vector3 movement = Actions.GetVector (action);

			Move (movement);
		}

	}

	void Move(Vector3 movement)
	{
		Vector3 new_pos = transform.position + movement * cellSize;

		// Check if there is a crate in the new position
		foreach (GameObject crate in crates) {
			if (crate.transform.position == new_pos)
			{
				// Move crate
				crate.transform.position += movement * cellSize;
			}
		}

		// Move player
		transform.position += movement * cellSize;
	}
}
