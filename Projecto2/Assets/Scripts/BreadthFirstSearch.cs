﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BreadthFirstSearch : SearchAlgorithm {

	private Queue<SearchNode> openQueue = new Queue<SearchNode> ();
	private HashSet<object> closedSet = new HashSet<object> ();


	void Start () 
	{
		Debug.Log("Breadth First Search");

		problem = GameObject.Find ("Map").GetComponent<Map> ().GetProblem();
		SearchNode start = new SearchNode (problem.GetStartState (), 0);
		openQueue.Enqueue (start);
		closedSet.Add (start.state); // Optimization
	}
	
	protected override void Step()
	{
		if (openQueue.Count > 0)
		{
			if(maxDatasetSize < openQueue.Count + closedSet.Count)
				maxDatasetSize = openQueue.Count + closedSet.Count;
			
			SearchNode cur_node = openQueue.Dequeue();
			
			if (debug) {
				SokobanState state = (SokobanState)cur_node.state;
				Debug.Log (state.player);
				Object last = GameObject.FindGameObjectWithTag ("Debug");
				GameObject.Destroy (last);
				Instantiate (myPrefab, state.player * GameObject.Find ("Map").GetComponent<Map> ().cellSize, Quaternion.identity);
			}

			if (problem.IsGoal (cur_node.state)) {
				solution = cur_node;
				finished = true;
				running = false;
			} else {
				Successor[] sucessors = problem.GetSuccessors (cur_node.state);
				foreach (Successor suc in sucessors) {
					if (!closedSet.Contains(suc.state)) {
						SearchNode new_node = new SearchNode (suc.state, suc.cost + cur_node.g, suc.action, cur_node);
						openQueue.Enqueue (new_node);
						closedSet.Add (suc.state); // Optimization
					}
				}
			}
		}
		else
		{
			finished = true;
			running = false;
		}
	}
}
