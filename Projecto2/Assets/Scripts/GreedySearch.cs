﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*
public class myComparerh:IComparer<SearchNode>
{
	public int Compare( SearchNode a, SearchNode b)
	{
		//				 h(a)   -   h(b)
		return (int) ((a.f-a.g) - (b.f-b.g));
	}
}	*/

public class GreedySearch : SearchAlgorithm {

	private priorityQueue<SearchNode> openQueue = new priorityQueue<SearchNode> ();
	private Dictionary<object,int> closedSet = new Dictionary<object, int> ();
	/*private myComparerh c;*/
	public List heuristic; // First Heuristic, Admissible, Non-admissible
	private int h;
	private int hv; // heuristic value = 0 -> FirstHeuristic, hv = 1 -> AdmissibleHeuristic, hv = 2 -> NA Heuristic 

	void Start () 
	{
		if (heuristic.ToString ().Equals ("First")) {
			hv = 0;
			Debug.Log("Greedy Search with First Heuristic");
		}
		else {
			if (heuristic.ToString ().Equals ("Admissible")) {
				hv = 1;
				Debug.Log ("Greedy Search with Admissible Heuristic");
			} else {
				hv = 2;
				Debug.Log("Greedy Search with Non-admissible Heuristic");
			}
		}

		problem = GameObject.Find ("Map").GetComponent<Map> ().GetProblem();
		object state = (object)problem.GetStartState ();

		h = problem.getHeuristic (state,hv);
		SearchNode start = new SearchNode (state, 0,h);
		/*c = new myComparerh ();*/
		openQueue.Enqueue (start, h);
		closedSet.Add (state, 0);
	}
	
	protected override void Step()
	{
		if (openQueue.Count > 0)
		{
			if(maxDatasetSize < openQueue.Count + closedSet.Count)
				maxDatasetSize = openQueue.Count + closedSet.Count;
			
			SearchNode cur_node = openQueue.Dequeue();

			if (debug) {
				SokobanState state = (SokobanState)cur_node.state;
				Debug.Log (state.player);
				Object last = GameObject.FindGameObjectWithTag ("Debug");
				GameObject.Destroy (last);
				Instantiate (myPrefab, state.player * GameObject.Find ("Map").GetComponent<Map> ().cellSize, Quaternion.identity);
			}

			// If algorithm not optimized and state not blocked, block when visited
			if (!optimized && !closedSet.ContainsKey (cur_node.state))
				closedSet.Add (cur_node.state, cur_node.depth);

			if (problem.IsGoal (cur_node.state)) {
				solution = cur_node;
				finished = true;
				running = false;


			} else {
				Successor[] sucessors = problem.GetSuccessors (cur_node.state);
				foreach (Successor suc in sucessors) {
					
					// If the depth distance between the cur_node state and its suc is greater than 1, then the last one hasn't been visited (yet) i.e. saves the shortest path
					if(closedSet.ContainsKey (suc.state) && closedSet[suc.state] > cur_node.depth +1){
						closedSet [suc.state] = cur_node.depth + 1;
						h = problem.getHeuristic (suc.state, hv);
						//                                                  cost = 1f
							SearchNode new_node = new SearchNode (suc.state, suc.cost + cur_node.g, h , suc.action, cur_node);

						// Sorts the list by heuristic in asceding order
						openQueue.Enqueue(new_node, h);

							
					}
					if (!closedSet.ContainsKey (suc.state)) {
						h = problem.getHeuristic (suc.state, hv);
						//                                               cost = 1f
						SearchNode new_node = new SearchNode (suc.state, suc.cost + cur_node.g, h , suc.action, cur_node);
						
						// Sorts the list by heuristic in asceding order
						openQueue.Enqueue(new_node, h);
							
						// If algorithm optimized, block when visited
						if(optimized)
							closedSet.Add (suc.state, new_node.depth);
					}
				}
			}
		}
		else
		{
			finished = true;
			running = false;
		}
	}
	public enum List {First, Admissible, NonAdmissible}
}
