Para correr o projecto:
- Abrir a pasta 'Projecto2' atraves do Unity.

Caso ocorra algum problema:
- Criar novo projecto Unity e importar o Projecto2.unitypackage


O projecto contem 7 cenarios:
- 5 algoritmos, 1 cenario por algoritmo -> preferivel para debugs e testes de otimiza��o
- TestAll, correr todos os algoritmos no mesmo mapa -> preferivel para obter apenas resultados de algoritmos especificos, estando optimizados ou n�o
- SpeedyTestAll, correr todos os algoritmos no mesmo mapa -> preferivel para obter APENAS resultados de algoritmos especificos j� estando optimizados e sem verifica��es inuteis (n�o ha debugs e so ha uma heuristica de Greedy e AStar por script)

Nota: Para melhor visualiza��o dos resultados, o programa tambem cria ficheiros de texto por mapa executado, contendo a informa��o pretendida. Para reset de valores guardados, � necessario apagar o ficheiro manualmente, caso contrario os ultimos resultados estar�o no final do documento.
